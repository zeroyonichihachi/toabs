toabs
=====

```
$ pwd
/private/tmp/abs

$ ls test
example.txt

$ toabs test/example.txt
/private/tmp/abs/test/example.txt
```

(tested `go version go1.7.1 darwin/amd64`)

[NYSL](http://www.kmonos.net/nysl/index.en.html)
