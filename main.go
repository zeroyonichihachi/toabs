package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
)

func main() {
	errlog := log.New(os.Stderr, "", 0)

	args := os.Args

	if len(args) != 2 {
		errlog.Println("[Error]: single file name should be specified.")
		os.Exit(1)
	}

	filename := os.Args[1]

	if _, err := os.Stat(filename); err != nil {
		errlog.Println("[Error]: ", err)
		os.Exit(1)
	}

	abspath, err := filepath.Abs(filename)
	if err != nil {
		errlog.Println("[Error]: ", err)
	}

	fmt.Println(abspath)
	os.Exit(0)
}
